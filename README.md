# Getting Started with SE Digital Blog App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Build steps

### 0. Create .env file.
```
PLEASE USE .env FROM ZIP FILE
```
Copy .env file to 'se-digital-blog' folder.

## There are two options to build app. (Select one from below)
1. [Dockerfile](#option-1-dockerfile)
2. [NPM or Yarn](#option-2-npm-or-yarn)     

---

## Option 1 Dockerfile   

### 1. Build docker container

```
docker build --tag se-digital-blog .
```


### 2. Run container

```
docker run -p 5000:80 se-digital-blog
```

Or run in detach mode.

```
docker run -p 5000:80 -d se-digital-blog
```


### 3. Navigate to [http://localhost:5000](http://localhost:5000)  

---
## Option 2 NPM or Yarn

### 1. Install dependencies

```
npm install
```
or 
```
yarn
```

### 2. Build 

```
npm run build
```
or
```
yarn build
```

### 3. Install static server
```
npm install -g serve
```
or
```
yarn global add server
```

### 4. Serve build file
```
serve -s build
```

### 5. Navigate to [http://localhost:5000](http://localhost:5000)

---
## About the app
1. [Blog List Page](http://localhost:5000)
2. [Blog Detail Page](http://localhost:5000/blogs/y4qgUGXXX7wEWKW92oHO)
3. [Blog Create Page](http://localhost:5000/blogs/create)
4. [404 Page](http://localhost:5000/not-found)
---
## Dependencies
1. Integrate with [Pixabay API](https://pixabay.com/api/docs/) free images.
2. RichTextEditor with [draft.js](https://draftjs.org/).
3. [React hook form](https://react-hook-form.com/).
4. Sanitize DOM with and prevent XSS with [dompurify](https://github.com/cure53/DOMPurify).
5. [Tailwind](https://tailwindcss.com) as CSS Framework.