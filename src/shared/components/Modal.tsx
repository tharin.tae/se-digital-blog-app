import classnames from "classnames";
import { useEffect } from "react";

type Props = {
  visible: boolean;
  children: React.ReactNode;
  onClose?: () => void;
};

const Modal: React.FC<Props> = ({ visible = false, children, onClose }) => {
  useEffect(() => {
    visible
      ? (document.body.className = "overflow-hidden")
      : (document.body.className = "");
  }, [visible]);

  return (
    <div
      className={classnames({
        "fixed z-30 inset-0 overflow-y-auto": true,
        hidden: !visible,
      })}
    >
      <div className="flex items-start justify-center min-h-screen pt-24">
        <div className="fixed inset-0 transition-opacity" aria-hidden="true">
          <div
            className="absolute inset-0 bg-black opacity-75"
            onClick={onClose}
          ></div>
        </div>

        <span
          className="hidden"
          aria-hidden="true"
        />

        <div
          className="inline-block bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all"
          role="dialog"
          aria-modal="true"
        >
          {children}
        </div>
      </div>
    </div>
  );
};

export default Modal;
