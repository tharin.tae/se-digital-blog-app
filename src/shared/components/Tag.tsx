import classnames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTag } from "@fortawesome/free-solid-svg-icons";

type Props = {
  tag: string;
  className?: string;
  onClick?: () => void;
};

const Tag: React.FC<Props> = ({ tag, className = "", onClick }) => {
  return (
    <button
      type="button"
      className={classnames({
        "focus:outline-none text-xs inline-flex items-center font-medium uppercase px-3 py-1 bg-green-200 text-green-800 rounded-full cursor-pointer": true,
        [className]: true,
      })}
      onClick={onClick}
    >
      <FontAwesomeIcon size="sm" icon={faTag} />
      <span className="ml-2">{tag}</span>
    </button>
  );
};

export default Tag;
