import classnames from "classnames";
type Props = {
  text?: string;
};

const InputErrorText: React.FC<Props> = ({ text }) => {
  return (
    <p
      className={classnames({
        "text-red-600 text-xs font-medium italic mt-1 h-4 opacity-0 transition-opacity": true,
        "opacity-100": text,
      })}
    >
      {text}
    </p>
  );
};

export default InputErrorText;
