import React from "react";
import classnames from "classnames";

type Props = {
  name: string;
  label?: string;
  hint?: string;
  value?: string;
  required?: boolean | undefined;
  isInvalid?: boolean;
  className?: string;
  placeholder?: string;
  children?: React.ReactNode;
  rows?: number;
};

const InputTextArea = React.forwardRef<HTMLTextAreaElement, Props>(
  (
    {
      rows = 4,
      name,
      value,
      label,
      hint,
      className,
      placeholder,
      required,
      isInvalid,
      children,
    },
    ref
  ) => (
    <div className={className}>
      <label className="block text-sm font-medium text-gray-600 h-5">
        {required && <span className="text-red-500 mr-1">*</span>}
        {label}
      </label>
      <textarea
        rows={rows}
        className={classnames({
          "outline-none w-full px-5 py-1 mt-1 text-gray-700 bg-gray-200 rounded focus:ring-2 focus:ring-blue-400 resize-none": true,
          "ring-1 ring-red-400": isInvalid,
        })}
        ref={ref}
        id={name}
        name={name}
        placeholder={placeholder}
      />
      <div className="h-5 text-gray-300 text-sm">{hint}</div>
      {children ? children : <div className="h-4" />}
    </div>
  )
);
export default InputTextArea;
