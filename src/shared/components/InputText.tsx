import React from "react";
import classnames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

type Props = {
  name: string;
  label?: string;
  hint?: string;
  value?: string;
  required?: boolean | undefined;
  isInvalid?: boolean;
  className?: string;
  placeholder?: string;
  children?: React.ReactNode;
};

const InputText = React.forwardRef<HTMLInputElement, Props>(
  (
    {
      name,
      value,
      label,
      hint,
      className,
      placeholder,
      required,
      isInvalid,
      children,
    },
    ref
  ) => (
    <div className={className}>
      <label className="block text-sm font-medium text-gray-600 h-5">
        {required && <span className="text-red-600 mr-1">*</span>}
        {label}
      </label>
      <div className="relative">
        <input
          className={classnames({
            "outline-none w-full px-5 py-1 mt-1 text-gray-700 bg-gray-100 rounded focus:placeholder-gray-300 transition": true,
            "ring-2 ring-red-600": isInvalid,
          })}
          type="text"
          ref={ref}
          id={name}
          name={name}
          placeholder={placeholder}
        />
        <div
          className={classnames({
            "absolute top-2 right-3 text-red-600 transition-opacity opacity-0": true,
            'opacity-100': isInvalid,
          })}
        >
          <FontAwesomeIcon icon={faTimes} />
        </div>
      </div>
      <div className="text-gray-300 text-sm mb-1">{hint}</div>
      {children ? children : <div className="h-4 mt-1" />}
    </div>
  )
);
export default InputText;
