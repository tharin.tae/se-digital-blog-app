import classnames from "classnames";
import "./NavLoading.css";

type Props = {
  isShow?: boolean;
  className?: string;
  classNameProgressBar?: string;
};

const NavLoading: React.FC<Props> = ({
  isShow = false,
  className = "",
  classNameProgressBar = "",
}) => {
  if (!isShow) {
    return <></>;
  }

  return (
    <div
      className={classnames({
        "leading-none w-full overflow-hidden fluentProgressBar-container": true,
        [className]: true,
      })}
    >
      <div
        className={classnames({
          "w-1/2 inline-block relative fluentProgressBar-waiting h-1": true,
          [classNameProgressBar]: true,
        })}
      ></div>
    </div>
  );
};

export default NavLoading;
