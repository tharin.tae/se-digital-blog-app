import { useEffect, useState } from "react";
import noimage from "../../no-image.png";

type Props = {
  src: string | undefined;
  alt?: string;
  className?: string;
};

const ImageFallback: React.FC<Props> = ({ src, alt, className }) => {
  const [localSrc, setLocalSrc] = useState(src);

  useEffect(() => {
    setLocalSrc(src);
  }, [src]);

  const handleError = () => {
    setLocalSrc(noimage);
  };

  return (
    <img
      className={className}
      src={localSrc}
      alt={alt}
      onError={handleError}
    ></img>
  );
};

export default ImageFallback;
