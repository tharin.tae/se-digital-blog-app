import { Link, NavLink } from "react-router-dom";

const NavBar: React.FC = () => {
  return (
    <nav className="fixed w-full flex items-center bg-white py-2.5 px-3 flex-wrap z-20 shadow-md">
      <Link to="/" className="p-2 mr-4 inline-flex items-center">
        <svg
          viewBox="0 0 24 24"
          xmlns="http://www.w3.org/2000/svg"
          className="fill-current text-gray-700 h-8 w-8 mr-2"
        >
          <path d="M12.001 4.8c-3.2 0-5.2 1.6-6 4.8 1.2-1.6 2.6-2.2 4.2-1.8.913.228 1.565.89 2.288 1.624C13.666 10.618 15.027 12 18.001 12c3.2 0 5.2-1.6 6-4.8-1.2 1.6-2.6 2.2-4.2 1.8-.913-.228-1.565-.89-2.288-1.624C16.337 6.182 14.976 4.8 12.001 4.8zm-6 7.2c-3.2 0-5.2 1.6-6 4.8 1.2-1.6 2.6-2.2 4.2-1.8.913.228 1.565.89 2.288 1.624 1.177 1.194 2.538 2.576 5.512 2.576 3.2 0 5.2-1.6 6-4.8-1.2 1.6-2.6 2.2-4.2 1.8-.913-.228-1.565-.89-2.288-1.624C10.337 13.382 8.976 12 6.001 12z" />
        </svg>
        <span className="text-xl text-gray-700 font-bold uppercase tracking-wide">
          Blog App
        </span>
      </Link>
      <div
        className="hidden top-navbar w-full lg:inline-flex lg:flex-grow lg:w-auto"
        id="navigation"
      >
        <div className="lg:inline-flex lg:flex-row lg:ml-auto lg:w-auto w-full lg:items-center items-start  flex flex-col lg:h-auto">
          <NavLink
            to="/"
            exact
            className="transition box-border font-bold lg:inline-flex lg:w-auto w-full px-5 py-2 rounded border-2 border-white text-green-800 hover:text-green-500 items-center justify-center  mr-4"
            activeClassName="box-content border-2 border-green-400 text-green-400 bg-green-100 text-green-500"
          >
            <span>List</span>
          </NavLink>
          <NavLink
            to="/blogs/create"
            className="transition font-medium bg-green-400 lg:inline-flex lg:w-auto w-full px-3 py-2 rounded text-green-800 items-center justify-center hover:bg-green-500 hover:text-white"
            activeClassName="bg-green-500 shadow shadow-inner"
          >
            <span>Create</span>
          </NavLink>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
