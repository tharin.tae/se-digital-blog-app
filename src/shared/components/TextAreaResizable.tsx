import classnames from "classnames";
import { ChangeEvent, useState } from "react";
type Props = {
  value: string;
  placeholder?: string;
  isInvalid?: boolean;
  minRows?: number;
  maxRows?: number;
  onChange?: (value: ChangeEvent<HTMLTextAreaElement>) => void;
};

const TextAreaResizable: React.FC<Props> = ({
  value = "",
  placeholder,
  minRows = 3,
  maxRows = 99,
  isInvalid,
  onChange,
}) => {
  const [rows, setRows] = useState(5);

  const handleChange = (event: ChangeEvent<HTMLTextAreaElement>) => {
    const textareaLineHeight = 16;

    const previousRows = event.target.rows;
    event.target.rows = minRows; // reset number of rows in textarea

    const currentRows = ~~(event.target.scrollHeight / textareaLineHeight);

    if (currentRows === previousRows) {
      event.target.rows = currentRows;
    }

    if (currentRows >= maxRows) {
      event.target.rows = maxRows;
      event.target.scrollTop = event.target.scrollHeight;
    }

    if (onChange) {
      onChange(event);
    }
    setRows(currentRows < maxRows ? currentRows : maxRows);
  };

  return (
    <textarea
      className={classnames({
        "relative rounded outline-none w-full py-1 mt-1 resize-none": true,
        "text-gray-700 bg-gray-200": true,
        "ring-2 ring-red-400": isInvalid,
      })}
      placeholder={placeholder}
      rows={rows}
      value={value}
      onChange={handleChange}
    />
  );
};

export default TextAreaResizable;
