import React, { ChangeEvent, useState } from "react";
import classnames from "classnames";
import { sanitize } from "dompurify";
import { DraftEditorCommand, Editor, EditorState, RichUtils } from "draft-js";
import { convertToHTML, convertFromHTML } from "draft-convert";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBold,
  faUnderline,
  faItalic,
  faCode,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import ButtonToolbar from "./ButtonToolbar";
import "./RichTextEditor.css";
import "draft-js/dist/Draft.css";

type Props = {
  value: EditorState;
  label?: string;
  hint?: string;
  required?: boolean | undefined;
  isInvalid?: boolean;
  className?: string;
  placeholder?: string;
  children?: React.ReactNode;
  onChange: (state: EditorState) => void;
  onBlur: () => void;
};

const RichTextEditor = React.forwardRef<any, Props>(
  (
    {
      value,
      label,
      hint,
      className,
      placeholder,
      required,
      isInvalid,
      children,
      onChange,
      onBlur,
    },
    ref
  ) => {
    const [isHtml, setIsHtml] = useState(false);
    const [html, setHtml] = useState(
      convertToHTML({})(value.getCurrentContent())
    );
    const currentStyles = value.getCurrentInlineStyle();

    const handleStyleChange = (style: string) => {
      if (onChange) {
        const newState: EditorState = RichUtils.toggleInlineStyle(value, style);
        onChange(newState);
        const html = convertToHTML({})(newState.getCurrentContent());
        setHtml(html);
      }
    };

    const handleKeyCommand = (
      command: DraftEditorCommand,
      editorState: EditorState
    ) => {
      if (onChange) {
        const newState: EditorState | null = RichUtils.handleKeyCommand(
          editorState,
          command
        );

        if (newState) {
          onChange(newState);
          const html = convertToHTML({})(newState.getCurrentContent());
          setHtml(html);
          return "handled";
        }
      }
      return "not-handled";
    };

    const handleHtmlChange = (event: ChangeEvent<HTMLTextAreaElement>) => {
      const { value } = event.target;
      const sanitized: string = sanitize(value);
      const editorState: EditorState = EditorState.createWithContent(
        convertFromHTML({})(sanitized)
      );
      onChange(editorState);
      setHtml(value);
    };

    const handleEditorChange = (state: EditorState) => {
      onChange(state);
      const html = convertToHTML({})(state.getCurrentContent());
      setHtml(html);
    };

    return (
      <div className={className}>
        <label className="block text-sm font-medium text-gray-600 h-5">
          {required && <span className="text-red-600 mr-1">*</span>}
          {label}
        </label>
        <div className="mr-2 border inline-block rounded divide-x divide-x-2 divide-gray-300 mt-2">
          <ButtonToolbar
            icon={faCode}
            active={isHtml}
            onClick={() => setIsHtml(!isHtml)}
          />
        </div>
        <div
          className={classnames({
            "border inline-block rounded divide-x divide-x-2 divide-gray-300 mt-2": true,
            hidden: isHtml,
          })}
        >
          <ButtonToolbar
            icon={faBold}
            active={currentStyles.has("BOLD")}
            onClick={() => handleStyleChange("BOLD")}
          />
          <ButtonToolbar
            icon={faItalic}
            active={currentStyles.has("ITALIC")}
            onClick={() => handleStyleChange("ITALIC")}
          />
          <ButtonToolbar
            icon={faUnderline}
            active={currentStyles.has("UNDERLINE")}
            onClick={() => handleStyleChange("UNDERLINE")}
          />
        </div>
        <div className="relative ">
          {isHtml ? (
            <textarea
              className={classnames({
                "relative rounded outline-none w-full px-5 py-1 mt-1 resize-y transition": true,
                "text-gray-700 bg-gray-100": true,
                "ring-2 ring-red-600": isInvalid,
              })}
              rows={10}
              value={html}
              onChange={handleHtmlChange}
            />
          ) : (
            <div
              className={classnames({
                "rounded outline-none w-full px-5 py-1 mt-1 resize-none transition": true,
                "text-gray-700 bg-gray-100": true,
                "ring-2 ring-red-600": isInvalid,
              })}
            >
              <Editor
                ref={ref}
                placeholder={placeholder}
                editorState={value}
                handleKeyCommand={handleKeyCommand}
                onBlur={onBlur}
                onChange={handleEditorChange}
              />

              <div
                className={classnames({
                  "absolute top-1 right-3 text-red-400": true,
                  hidden: !isInvalid,
                })}
              >
                <FontAwesomeIcon icon={faTimes} />
              </div>
            </div>
          )}
          <div
            className={classnames({
              "absolute top-1 right-3 text-red-600": true,
              hidden: !isInvalid,
            })}
          >
            <FontAwesomeIcon icon={faTimes} />
          </div>
        </div>

        <div className="text-gray-300 text-sm mt-1">{hint}</div>
        {children ? children : <div className="h-4" />}
      </div>
    );
  }
);

export default RichTextEditor;
