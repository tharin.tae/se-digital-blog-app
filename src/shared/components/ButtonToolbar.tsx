import classnames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconProp } from "@fortawesome/fontawesome-svg-core";

type Props = {
  icon: IconProp;
  active?: boolean;
  onClick?: () => void;
};

const ButtonToolbar: React.FC<Props> = ({ icon, active, onClick }) => {
  return (
    <button
      type="button"
      onClick={onClick}
      className={classnames({
        "px-3 focus:outline-none border-green-100 hover:bg-green-200 cursor-pointer text-gray-500": true,
        "text-green-500 bg-green-100 shadow-inner": active,
      })}
    >
      <FontAwesomeIcon icon={icon} />
    </button>
  );
};

export default ButtonToolbar;
