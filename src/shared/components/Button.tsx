import classnames from "classnames";
import { ReactNode } from "react";

type Props = {
  type?: "button" | "submit" | "reset" | undefined;
  ghost?: boolean;
  disabled?: boolean;
  className?: string;
  children?: ReactNode;
  onClick?: () => void;
};

const Button: React.FC<Props> = ({
  type = "button",
  className = "",
  ghost,
  disabled,
  children,
  onClick,
}) => {
  return (
    <button
      disabled={disabled}
      className={classnames({
        "focus:outline-none flex px-3 shadow rounded items-center justify-center transition ease-in-out": true,
        "bg-green-400 hover:bg-green-500 hover:text-white text-green-800": !ghost && !disabled,
        "hover:bg-green-400 hover:bg-opacity-20 text-green-500 border border-green-500": ghost,
        "bg-green-300": disabled,
        "cursor-default": disabled,
        [className]: true,
      })}
      type={type}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default Button;
