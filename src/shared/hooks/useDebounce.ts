import { useCallback, useState } from "react";
import { debounce } from "lodash";

export const useDebounce = (obj: any = null, wait: number = 1000) => {
  const [state, setState] = useState(obj);

  const setDebouncedState = (_val: any) => {
    _debounce(_val);
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const _debounce = useCallback(
    debounce((_prop: string) => {
      setState(_prop);
    }, wait),
    []
  );

  return [state, setDebouncedState];
};
