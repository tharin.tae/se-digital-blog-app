import { combineReducers } from "@reduxjs/toolkit";
import { connectRouter } from "connected-react-router";
import { History } from "history";
import blogReducer from "../features/blog/blogSlice";
import imageReducer from "../features/images/imageSlice";
import notificationReducer from "../features/notification/notificationSlice";

export const createRootReducer = (history: History) =>
  combineReducers({
    router: connectRouter(history),
    blogs: blogReducer,
    images: imageReducer,
    notifications: notificationReducer,
  });
