import React from "react";
import { Route, Switch } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router";
import { history } from "./lib/store";
import BlogCreate from "./features/blog/containers/BlogCreate";
import BlogDetail from "./features/blog/containers/BlogDetail";
import BlogList from "./features/blog/containers/BlogList";
import NotificationList from "./features/notification/contianers/NotificationList";
import NavBar from "./shared/components/NavBar";
import NotFound from "./shared/components/NotFound";

function App() {
  return (
    <ConnectedRouter history={history}>
      <NotificationList />
      <NavBar />
      <Switch>
        <Route exact path="/">
          <BlogList />
        </Route>
        <Route exact path="/blogs/create">
          <BlogCreate />
        </Route>
        <Route path="/blogs/:id">
          <BlogDetail />
        </Route>
        <Route path="*">
          <NotFound />
        </Route>
      </Switch>
    </ConnectedRouter>
  );
}

export default App;
