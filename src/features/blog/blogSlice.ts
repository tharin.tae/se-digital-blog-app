import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
  nanoid,
  PayloadAction,
} from "@reduxjs/toolkit";
import { normalize } from "normalizr";
import { push } from "connected-react-router";
import { LOADING_STATE } from "../../shared/enums/loadingState.enum";
import { NormalizeState, RootState } from "../../lib/store";
import { blogService } from "./blogService";
import { Blog } from "./blogModel";
import { blogEntity } from "./blogSchema";
import {
  Notification,
  NotificationType,
  showNotification,
} from "../notification/notificationSlice";

const blogAdapter = createEntityAdapter<Blog>();

type NormalizeBlog = {
  blogs: {
    [key: string]: Blog;
  };
};

export type CreateBlogPayload = {
  title: string;
  description: string;
  imageUrl?: string;
};

// Async Action
export const getBlogAction = createAsyncThunk(
  "blogs/getBlog",
  async (id: string, { getState, dispatch }): Promise<Blog | undefined> => {
    let blog: Blog | undefined = (getState() as RootState).blogs.entities[id];

    if (!blog) {
      try {
        const fecthedBlog: Blog = await blogService.fetchById(id);
        blog = fecthedBlog;
      } catch (e) {
        dispatch(push("/not-found"));
      }
    }

    return blog;
  }
);

export const getBlogListAction = createAsyncThunk(
  "blogs/getBlogList",
  async (): Promise<NormalizeBlog> => {
    const blogs: Blog[] = await blogService.fetchAll();
    const normalized = normalize<NormalizeBlog, NormalizeState>(blogs, [
      blogEntity,
    ]);
    return normalized.entities;
  }
);

export const createBlogAction = createAsyncThunk(
  "blogs/createBlog",
  async (payload: CreateBlogPayload, { dispatch }): Promise<Blog> => {
    const blog: Blog = await blogService.create(payload);

    const notification: Notification = {
      id: nanoid(),
      type: NotificationType.SUCCESS,
      title: "Success",
      body: "Blog has been created",
    };

    await dispatch(showNotification(notification));
    await dispatch(push("/"));
    return blog;
  }
);

// Slice
const blogSlice = createSlice({
  name: "blogs",
  initialState: blogAdapter.getInitialState({
    list: {
      status: LOADING_STATE.IDLE,
    },
    create: {
      status: LOADING_STATE.IDLE,
    },
    detail: {
      status: LOADING_STATE.IDLE,
    },
  }),
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getBlogAction.pending, (state) => {
      state.detail.status = LOADING_STATE.PENDING;
    });
    builder.addCase(getBlogAction.rejected, (state) => {
      state.detail.status = LOADING_STATE.FAILED;
    });
    builder.addCase(
      getBlogAction.fulfilled,
      (state, action: PayloadAction<Blog | undefined>) => {
        state.detail.status = LOADING_STATE.SUCCEEDED;
        action.payload && blogAdapter.upsertOne(state, action.payload);
      }
    );
    builder.addCase(getBlogListAction.pending, (state) => {
      state.list.status = LOADING_STATE.PENDING;
    });
    builder.addCase(getBlogListAction.rejected, (state) => {
      state.list.status = LOADING_STATE.FAILED;
    });
    builder.addCase(
      getBlogListAction.fulfilled,
      (state, action: PayloadAction<NormalizeBlog>) => {
        state.list.status = LOADING_STATE.SUCCEEDED;
        blogAdapter.upsertMany(state, action.payload.blogs || {});
      }
    );
    builder.addCase(createBlogAction.pending, (state) => {
      state.create.status = LOADING_STATE.PENDING;
    });
    builder.addCase(createBlogAction.rejected, (state) => {
      state.create.status = LOADING_STATE.FAILED;
    });
    builder.addCase(
      createBlogAction.fulfilled,
      (state, action: PayloadAction<Blog>) => {
        state.create.status = LOADING_STATE.SUCCEEDED;
        blogAdapter.upsertOne(state, action.payload);
      }
    );
  },
});

// Selector
export const {
  selectById: selectBlogById,
  selectIds: selectBlogIds,
  selectEntities: selectBlogEntities,
  selectAll: selectAllBlogs,
  selectTotal: selectTotalBlogs,
} = blogAdapter.getSelectors<RootState>((state) => state.blogs);

export default blogSlice.reducer;
