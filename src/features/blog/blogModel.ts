export interface Blog {
  id: string;
  created: number;
  description: string;
  imageUrl?: string;
  title: string;
}
