import { AxiosResponse } from "axios";
import httpClient from "../../lib/httpClient";
import { Blog } from "./blogModel";
import { CreateBlogPayload } from "./blogSlice";

const BLOG_API = "/blog";

export const blogService = {
  async create(payload: CreateBlogPayload): Promise<Blog> {
    const { data }: AxiosResponse<Blog> = await httpClient.post(
      BLOG_API,
      payload
    );

    return data;
  },
  async fetchById(id: string): Promise<Blog> {
    const { data }: AxiosResponse<Blog[]> = await httpClient.get(
      `${BLOG_API}/${id}`
    );

    const blog: Blog | undefined = data.find((blog: Blog) => blog.id === id);
    if (!blog) {
      throw new Error("No blog found");
    }
    return blog;
  },
  async fetchAll(): Promise<Blog[]> {
    const response: AxiosResponse<Blog[]> = await httpClient.get(BLOG_API);
    const blogs: Blog[] = response?.data;
    return blogs;
  },
};
