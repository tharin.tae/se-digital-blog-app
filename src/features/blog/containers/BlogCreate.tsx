import { Controller, useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { EditorState } from "draft-js";
import { convertToHTML } from "draft-convert";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSave } from "@fortawesome/free-solid-svg-icons";
import InputText from "../../../shared/components/InputText";
import RichTextEditor from "../../../shared/components/RichTextEditor";
import ModalImage from "../../images/containers/ModalImage";
import { useState } from "react";
import InputErrorText from "../../../shared/components/InputErrorText";
import Button from "../../../shared/components/Button";
import { createBlogAction, CreateBlogPayload } from "../blogSlice";
import { RootState } from "../../../lib/store";
import { LOADING_STATE } from "../../../shared/enums/loadingState.enum";
// import { useHistory } from "react-router-dom";
import ImageFallback from "../../../shared/components/ImageFallback";

type FormData = {
  title: string;
  description: EditorState;
  imageUrl?: string;
};

const urlRegex = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&//=]*)/;

export const BlogCreate: React.FC = () => {
  const dispatch = useDispatch();
  // const history = useHistory();
  const [visible, setVisible] = useState<boolean>(false);
  const createStatus = useSelector<RootState>(
    (state) => state.blogs.create.status
  );
  const {
    control,
    errors,
    watch,
    register,
    handleSubmit,
    setValue,
  } = useForm<FormData>({
    defaultValues: {
      title: "",
      description: EditorState.createEmpty(),
      imageUrl: "",
    },
  });

  const handleCreateBlog = async (data: FormData) => {
    const payload: CreateBlogPayload = {
      title: data.title,
      description: convertToHTML({})(data.description.getCurrentContent()),
      imageUrl: data.imageUrl,
    };
    await dispatch(createBlogAction(payload));
    // history.push("/");
  };

  const handleSelectImage = (imageUrl: string) => {
    setValue("imageUrl", imageUrl);
    setVisible(false);
  };

  return (
    <div className="flex flex-col m-auto py-28 container">
      <form
        className="mx-auto w-full max-w-6xl leading-loose p-10 bg-white rounded shadow-xl"
        onSubmit={handleSubmit(handleCreateBlog)}
      >
        <div className="flex">
          <h1 className="w-2/3 text-2xl mb-5">Add new blog</h1>
          <div className="w-1/3 ml-6">
            <Button
              disabled={createStatus === LOADING_STATE.PENDING}
              type="submit"
            >
              <FontAwesomeIcon icon={faSave} />
              <span className="ml-2">
                {createStatus === LOADING_STATE.PENDING ? "Saving" : "Save"}
              </span>
            </Button>
          </div>
        </div>
        <div className="flex">
          <div className="w-2/3">
            <InputText
              required
              name="title"
              label="Title"
              placeholder="Enter title"
              isInvalid={!!errors?.title}
              ref={register({ required: true })}
            >
              {errors.title && <InputErrorText text="Title is required." />}
            </InputText>
            <Controller
              name="description"
              control={control}
              rules={{
                validate: (editorState: EditorState) => {
                  return editorState.getCurrentContent().hasText();
                },
              }}
              render={({ value, onChange, onBlur, ref }) => (
                <RichTextEditor
                  required
                  className="mt-4"
                  label="Description"
                  placeholder="Enter description"
                  hint="If you want to pasted as HTML, Switch to code mode."
                  ref={ref}
                  value={value}
                  isInvalid={!!errors?.description}
                  onChange={onChange}
                  onBlur={onBlur}
                >
                  {errors.description && (
                    <InputErrorText text="Description is required." />
                  )}
                </RichTextEditor>
              )}
            />
          </div>
          <div className="w-1/3 ml-6">
            <ImageFallback className="mb-4" src={watch("imageUrl", "")} />
            <InputText
              name="imageUrl"
              label="Image"
              hint="Image url"
              placeholder="Enter image url"
              isInvalid={!!errors?.imageUrl}
              ref={register({ pattern: urlRegex })}
            >
              {errors.imageUrl && <InputErrorText text="Url format invalid." />}
            </InputText>
            <Button
              ghost
              className="mt-4 py-1 w-full"
              onClick={() => setVisible(true)}
            >
              Select image
            </Button>
          </div>
        </div>
      </form>
      <ModalImage
        visible={visible}
        onSelectImage={handleSelectImage}
        onClose={() => setVisible(false)}
      />
    </div>
  );
};

export default BlogCreate;
