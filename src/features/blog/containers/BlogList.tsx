import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import classnames from "classnames";
import { AppDispatch, RootState } from "../../../lib/store";
import { LOADING_STATE } from "../../../shared/enums/loadingState.enum";
import NavLoading from "../../../shared/components/NavLoading";
import { Blog } from "../blogModel";
import { getBlogListAction, selectAllBlogs } from "../blogSlice";
import BlogCard from "../components/BlogCard";

export const BlogList: React.FC = () => {
  const blogList = useSelector(selectAllBlogs);
  const loadingStatus = useSelector<RootState>(
    (state) => state.blogs.list.status
  );
  const dispatch = useDispatch<AppDispatch>();

  useEffect(() => {
    dispatch(getBlogListAction());
  }, [dispatch]);

  return (
    <div>
      <NavLoading
        className="absolute top-16"
        classNameProgressBar="h-3"
        isShow={loadingStatus === LOADING_STATE.PENDING}
      />
      <div
        className={classnames({
          "container m-auto pt-28 flex flex-wrap": true,
          hidden: loadingStatus !== LOADING_STATE.SUCCEEDED,
        })}
      >
        {blogList.map((blog: Blog) => (
          <BlogCard
            className="mx-8 my-6"
            linkTo={`/blogs/${blog.id}`}
            key={blog.id}
            imgSrc={blog.imageUrl}
            title={blog.title}
            created={blog.created}
            description={blog.description}
          />
        ))}
      </div>
    </div>
  );
};

export default BlogList;
