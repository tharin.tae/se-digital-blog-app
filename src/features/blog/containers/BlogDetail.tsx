import dayjs from "dayjs";
import classnames from "classnames";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { sanitize } from "dompurify";
import { RootState } from "../../../lib/store";
import ImageFallback from "../../../shared/components/ImageFallback";
import { Blog } from "../blogModel";
import { getBlogAction, selectBlogById } from "../blogSlice";
import { useEffect } from "react";
import { LOADING_STATE } from "../../../shared/enums/loadingState.enum";
import NavLoading from "../../../shared/components/NavLoading";

type RouteParam = {
  id: string;
};

const BlogDetail: React.FC = () => {
  const dispatch = useDispatch();
  const { id } = useParams<RouteParam>();
  const blogStatus = useSelector(
    (state: RootState) => state.blogs.detail.status
  );
  const blog: Blog | undefined = useSelector((state: RootState) =>
    selectBlogById(state, id)
  );

  const sanitized: string = blog?.description
    ? sanitize(blog?.description)
    : "";

  useEffect(() => {
    dispatch(getBlogAction(id));
  }, [id, dispatch]);

  return (
    <div>
      <NavLoading
        className="absolute top-16"
        classNameProgressBar="h-3"
        isShow={blogStatus === LOADING_STATE.PENDING}
      />
      <div
        className={classnames({
          "container py-28 mx-auto": true,
          hidden: blogStatus === LOADING_STATE.PENDING,
        })}
      >
        <div className="mb-4 md:mb-0 w-full max-w-screen-sm mx-auto relative h-96">
          <div
            className="absolute left-0 bottom-0 w-full h-full z-10"
            style={{
              backgroundImage:
                "linear-gradient(180deg,transparent,rgba(0,0,0,.7))",
            }}
          ></div>
          <ImageFallback
            className="absolute left-0 top-0 w-full h-full z-0 object-cover"
            src={blog?.imageUrl}
            alt={blog?.title}
          />
          <div className="p-4 absolute bottom-0 left-0 z-20">
            <h2 className="text-2xl font-semibold text-gray-100 leading-tight">
              {blog?.title}
            </h2>
            <div className="flex mt-3">
              <div>
                <p className="font-semibold text-gray-400 text-sm">
                  {dayjs(blog?.created).format("MMM DD, YYYY")}
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="px-4 lg:px-0 mt-12 text-gray-700 max-w-screen-sm mx-auto text-lg leading-relaxed">
          <div
            className="space-y-8 text-gray-700"
            dangerouslySetInnerHTML={{ __html: sanitized }}
          ></div>
        </div>
      </div>
    </div>
  );
};

export default BlogDetail;
