import { useEffect, useRef } from "react";
import clamp from "clamp-js";
import dompurify from "dompurify";
import classnames from "classnames";
import { Link } from "react-router-dom";
import dayjs from "dayjs";
import ImageFallback from "../../../shared/components/ImageFallback";

type Props = {
  title: string;
  created: number;
  description: string;
  imgSrc?: string;
  className?: string;
  linkTo: string;
};

const BlogCard: React.FC<Props> = ({
  title,
  created,
  description = "",
  imgSrc = "",
  className = "",
  linkTo,
}) => {
  const titleEl = useRef<HTMLHeadingElement>(null);
  const descriptionEl = useRef<HTMLParagraphElement>(null);
  const sanitizedDescription = dompurify.sanitize(description);

  useEffect(() => {
    titleEl.current && clamp(titleEl.current, { clamp: 2 });
    descriptionEl.current && clamp(descriptionEl.current, { clamp: 3 });
  }, [title, description]);

  return (
    <div
      className={classnames({
        "blogs bg-white max-w-xs shadow hover:shadow-lg transition-shadow ease-in-out rounded relative": true,
        [className]: true,
      })}
    >
      <ImageFallback
        className="h-48 w-full object-cover rounded-t"
        src={imgSrc}
        alt={title}
      />
      <div className="p-5 mb-14">
        <div className="text-sm text-gray-400">
          {dayjs(created).format("MMM DD, YYYY")}
        </div>
        <h1 ref={titleEl} className="text-xl font-bold text-green-600">
          {title}
        </h1>
        <p
          ref={descriptionEl}
          className="text-sm text-gray-500 mt-4 flex-1"
          dangerouslySetInnerHTML={{ __html: sanitizedDescription }}
        />
      </div>
      <Link
        to={linkTo}
        className={classnames({
          "absolute bottom-5 right-5 py-2 mt-4 px-6 inline-block rounded ": true,
          "text-sm text-green-50 bg-green-400 hover:bg-green-500 cursor-pointer font-medium transition ease-in-out": true,
        })}
      >
        READ MORE
      </Link>
    </div>
  );
};
export default BlogCard;
