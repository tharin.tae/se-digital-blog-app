import axios, { AxiosResponse, CancelTokenSource } from "axios";
import { Image } from "./imageModel";

const IMAGE_API_KEY = process.env.REACT_APP_IMAGE_API_KEY;
const IMAGE_API = `${process.env.REACT_APP_IMAGE_API_URL}/?key=${IMAGE_API_KEY}`;
export const PER_PAGE = 30;

type ImageResponse = {
  total: number;
  totalHits: number;
  hits: Image[];
};

export type ImageMeta = {
  total: number;
  hits: number;
};

export type ImageData = {
  meta: ImageMeta;
  data: Image[];
};

let cancelToken: CancelTokenSource;

export const imageService = {
  async fetchAll(term: string = "", page: number = 1): Promise<ImageData> {
    if (cancelToken) {
      cancelToken?.cancel("Operation canceled due to new request.");
    }

    cancelToken = axios.CancelToken.source();

    const response: AxiosResponse<ImageResponse> = await axios.get(IMAGE_API, {
      cancelToken: cancelToken.token,
      params: {
        q: term,
        orientation: "horizontal",
        editors_choice: true,
        safesearch: true,
        per_page: PER_PAGE,
        page,
      },
    });
    const meta: ImageMeta = {
      total: response?.data?.total || 0,
      hits: response?.data?.totalHits || 0,
    };
    const images: Image[] = response?.data?.hits || [];
    return {
      meta,
      data: images,
    };
  },
};
