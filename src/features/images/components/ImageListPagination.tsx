import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";

import Button from "../../../shared/components/Button";

type Props = {
  hasPrevious: boolean;
  hasNext: boolean;
  currentPage: number;
  totalPage: number;
  onSetPage: (page: number) => void;
};

const ImageListPagination: React.FC<Props> = ({
  hasPrevious,
  hasNext,
  currentPage = 1,
  totalPage = 1,
  onSetPage,
}) => {
  const handleNext = () => {
    const nextPage = currentPage + 1;
    onSetPage(nextPage);
  };

  const handlePrevious = () => {
    const previousPage = currentPage - 1;
    onSetPage(previousPage);
  };

  return (
    <div className="flex justify-between items-center mt-2">
      <div className="flex-1">
        {hasPrevious && (
          <Button ghost className="px-4 py-1" onClick={handlePrevious}>
            <FontAwesomeIcon size="sm" icon={faChevronLeft} />
            <span className="ml-2">Previous</span>
          </Button>
        )}
      </div>
      <div className="inline-flex text-green-500 text-sm">
        {totalPage !== 0 && (
          <span>
            {currentPage} of {totalPage}
          </span>
        )}
      </div>
      <div className="flex-1">
        {hasNext && (
          <Button ghost className="ml-auto px-4 py-1" onClick={handleNext}>
            <span className="mr-2">Next</span>
            <FontAwesomeIcon size="sm" icon={faChevronRight} />
          </Button>
        )}
      </div>
    </div>
  );
};

export default ImageListPagination;
