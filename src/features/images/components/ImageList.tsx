import ImageItem from "./ImageItem";
import { Image } from "../imageModel";
import NavLoading from "../../../shared/components/NavLoading";
import { LOADING_STATE } from "../../../shared/enums/loadingState.enum";

type Props = {
  list: Image[];
  status: LOADING_STATE;
  selectedId?: number;
  onSelectImageId: (imageId: number) => void;
};

const ImageList: React.FC<Props> = ({
  list = [],
  status,
  selectedId,
  onSelectImageId,
}) => {
  return (
    <div>
      <div className="p-4">
        {status === LOADING_STATE.SUCCEEDED && list.length === 0 && (
          <div>
            <div className="text-white text-xl font-medium text-center mt-10 px-80 whitespace-nowrap">
              No result found!
            </div>
          </div>
        )}
        <div className="grid grid-cols-2 md:grid-cols-6 gap-2 text-center mt-2">
          {list.map((image: Image) => (
            <ImageItem
              key={image.id}
              imageSrc={image.previewURL}
              alt={image.tags}
              isSelected={selectedId === image.id}
              onClick={() => onSelectImageId(image.id)}
            />
          ))}
        </div>
        <div className="h-1">
          <NavLoading isShow={status === LOADING_STATE.PENDING} />
        </div>
      </div>
    </div>
  );
};

export default ImageList;
