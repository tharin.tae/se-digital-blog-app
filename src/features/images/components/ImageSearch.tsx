import { useState, ChangeEvent, useEffect } from "react";
import classnames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, faTimes } from "@fortawesome/free-solid-svg-icons";

type Props = {
  term: string;
  countFound: number;
  countMaximum: number;
  onSetTerm: (term: string) => void;
};

const ImageSearch: React.FC<Props> = ({
  term,
  onSetTerm,
  countFound,
  countMaximum,
}) => {
  const [localTerm, setLocalTerm] = useState<string>(term);

  const handleSearch = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    setLocalTerm(value);
    onSetTerm(value);
  };

  const handleClear = () => {
    setLocalTerm("");
    onSetTerm("");
  };

  useEffect(() => {
    setLocalTerm(term);
  }, [term]);

  return (
    <>
      <div className="relative text-gray-600">
        <input
          value={localTerm}
          placeholder="Search e.g. yellow, green, car, cat"
          className="w-full bg-white h-10 px-5 pr-10 rounded text-sm focus:outline-none"
          onChange={handleSearch}
        />
        <button
          type="button"
          className={classnames({
            "focus:outline-none absolute right-0.5 bottom-2 mt-3 mr-4 cursor-default": true,
            hidden: localTerm.length,
          })}
        >
          <FontAwesomeIcon className="text-gray-600" icon={faSearch} />
        </button>
        <button
          type="button"
          className={classnames({
            "focus:outline-none absolute right-0.5 bottom-2 mt-3 mr-4": true,
            hidden: !localTerm.length,
          })}
          onClick={handleClear}
        >
          <FontAwesomeIcon className="text-gray-600" icon={faTimes} />
        </button>
      </div>
      <div className="flex justify-between mt-1 text-sm text-gray-400">
        <div>
          <span className="font-bold text-gray-300 mr-1">{countFound}</span>
          <span>images found.</span>
        </div>
        <div>
          <span>Limit to </span>
          <span className="font-bold text-gray-300 mr-1"> {countMaximum}</span>
          <span>images search result.</span>
        </div>
      </div>
    </>
  );
};

export default ImageSearch;
