import classnames from "classnames";
import "./ImageItem.css";

type Props = {
  imageSrc: string;
  alt?: string;
  isSelected?: boolean;
  onClick?: () => void;
};

const ImageItem: React.FC<Props> = ({ imageSrc, alt, isSelected, onClick }) => {
  return (
    <img
      className={classnames({
        "ImageItem select-none rounded w-32 h-20 object-cover mx-auto": true,
        "ring-4 ring-green-500": isSelected,
      })}
      src={imageSrc}
      alt={alt}
      onClick={onClick}
    />
  );
};

export default ImageItem;
