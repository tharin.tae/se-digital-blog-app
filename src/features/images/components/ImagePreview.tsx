import classnames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faImage, faEye } from "@fortawesome/free-solid-svg-icons";
import { Image } from "../imageModel";
import Tag from "../../../shared/components/Tag";
import { useEffect, useState } from "react";
import Button from "../../../shared/components/Button";
import pixabay from "../../../pixabay.svg";

enum LOAD_STATUS {
  IDLE = "IDLE",
  LOADING = "LOADING",
  LOADED = "LOADED",
}

type Props = {
  selectedImage: Image | undefined;
  onSetTerm?: (term: string) => void;
  onSelectImage?: (imageUrl: string) => void;
};

const ImagePreview: React.FC<Props> = ({
  selectedImage,
  onSetTerm,
  onSelectImage,
}) => {
  const [status, setStatus] = useState(LOAD_STATUS.IDLE);
  const tags: string[] = selectedImage?.tags.split(", ") || [];

  const handleSelectImage = () => {
    if (onSelectImage) {
      onSelectImage(selectedImage?.webformatURL!);
    }
  };

  useEffect(() => {
    if (selectedImage?.id) {
      setStatus(LOAD_STATUS.LOADING);
    }
  }, [selectedImage?.id]);

  return (
    <div className="w-80 p-4 pt-12">
      {!selectedImage && (
        <div className="flex flex-col items-center justify-center rounded p-10 min-h-full">
          <FontAwesomeIcon className="text-gray-300" size="4x" icon={faImage} />
          <span className="text-gray-300 text-lg">No image selected</span>
        </div>
      )}

      {status === LOAD_STATUS.LOADING && (
        <div className="rounded-md w-full">
          <div className="animate-pulse">
            <div className="rounded bg-green-300 w-3/4 h-8 mx-auto"></div>
            <div className="rounded bg-green-300 w-full h-48 mt-4"></div>
            <div className="mx-auto h-2 bg-green-300 rounded w-3/4 mt-2"></div>
            <div className="space-y-2 mt-4">
              <div className="h-4 bg-green-300 rounded w-1/4"></div>
              <div className="flex space-x-2">
                <div className="h-6 bg-green-300 rounded-full w-3/12"></div>
                <div className="h-6 bg-green-300 rounded-full w-2/12"></div>
                <div className="h-6 bg-green-300 rounded-full w-4/12"></div>
              </div>
              <div className="flex space-x-2">
                <div className="h-6 bg-green-300 rounded-full w-5/12"></div>
                <div className="h-6 bg-green-300 rounded-full w-2/12"></div>
                <div className="h-6 bg-green-300 rounded-full w-2/12"></div>
              </div>
            </div>
          </div>
        </div>
      )}

      <div
        className={classnames({
          hidden: status !== LOAD_STATUS.LOADED || !selectedImage,
        })}
      >
        <img className="mx-auto h-8" src={pixabay} alt="Pixabay" />
        <div className="relative">
          <img
            className="w-full rounded mt-4"
            src={selectedImage?.webformatURL}
            alt={selectedImage?.tags}
            onLoad={() => setStatus(LOAD_STATUS.LOADED)}
          />
          <a
            rel="noreferrer"
            target="_blank"
            href={selectedImage?.pageURL}
            className="flex justify-center items-center absolute z-10 bg-black top-0 left-0 right-0 bottom-0 h-full w-full transition-opacity opacity-0 hover:opacity-60"
          >
            <div className="text-gray-300">
              <FontAwesomeIcon icon={faEye} />
              <span className="ml-2">View Image</span>
            </div>
          </a>
        </div>
        <div className="text-xs text-center mt-2">
          Image by: <span className="font-medium">{selectedImage?.user}</span>
        </div>
        <div className="font-medium mt-4 mb-2">Tags:</div>
        <div>
          {tags.map((tag: string) => (
            <Tag
              className="mr-1 mb-1"
              key={tag}
              tag={tag}
              onClick={() => onSetTerm && onSetTerm(tag)}
            />
          ))}
        </div>
      </div>
      <div className="flex space-x-2 mt-6">
        <Button className="w-full py-2" onClick={handleSelectImage}>
          SELECT
        </Button>
      </div>
    </div>
  );
};

export default ImagePreview;
