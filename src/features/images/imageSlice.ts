import {
  createAsyncThunk,
  createEntityAdapter,
  createSelector,
  createSlice,
  Dictionary,
  PayloadAction,
} from "@reduxjs/toolkit";
import { normalize, Schema } from "normalizr";
import { RootState } from "../../lib/store";
import { LOADING_STATE } from "../../shared/enums/loadingState.enum";
import { Image } from "./imageModel";
import { imageService, ImageData, ImageMeta, PER_PAGE } from "./imageService";
import { imageEntity } from "./imageSchema";

export type ImageState = {
  status: LOADING_STATE;
  currentIds: number[];
  selectedId: number | undefined;
  term: string;
  page: number;
  total: number;
  maximum: number;
  totalPage: number;
  hasPrevious: boolean;
  hasNext: boolean;
};

type ImageEntities = {
  images: Record<number, Image>;
};

type GetImageListFullfilled = {
  meta: ImageMeta;
  result: number[];
  entities: ImageEntities;
};

export type GetImageListPayload = {
  term: string;
  page: number;
};

// Adaptor
export const imageAdapter = createEntityAdapter<Image>();

// Async Actions
export const getImageListAction = createAsyncThunk(
  "images/getImageList",
  async (payload: GetImageListPayload): Promise<GetImageListFullfilled> => {
    const { term, page } = payload;
    const data: ImageData = await imageService.fetchAll(term, page);
    const { entities, result } = normalize<
      Schema<ImageEntities>,
      ImageEntities,
      number[]
    >(data.data, [imageEntity]);

    return {
      meta: data.meta,
      entities,
      result,
    };
  }
);

// Slice
const imageSlice = createSlice({
  name: "images",
  initialState: imageAdapter.getInitialState<ImageState>({
    status: LOADING_STATE.IDLE,
    currentIds: [],
    selectedId: undefined,
    term: "",
    page: 1,
    total: 0,
    maximum: 0,
    totalPage: 0,
    hasPrevious: false,
    hasNext: true,
  }),
  reducers: {
    setTerm(state, action: PayloadAction<string>) {
      state.page = 1;
      state.term = action.payload;
    },
    setPage(state, action: PayloadAction<number>) {
      state.page = action.payload;
    },
    setSelectImageId(state, action: PayloadAction<number | undefined>) {
      state.selectedId = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getImageListAction.pending, (state, action) => {
      state.status = LOADING_STATE.PENDING;
    });
    builder.addCase(getImageListAction.rejected, (state, action) => {
      state.status = LOADING_STATE.FAILED;
    });
    builder.addCase(
      getImageListAction.fulfilled,
      (state, action: PayloadAction<GetImageListFullfilled>) => {
        const { meta, result, entities } = action.payload;
        state.status = LOADING_STATE.SUCCEEDED;
        state.currentIds = [...result];
        state.total = meta.total;
        state.maximum = meta.hits;
        state.totalPage = Math.ceil(meta.hits / PER_PAGE);
        state.hasPrevious = state.page !== 1;
        state.hasNext = state.page * PER_PAGE < meta.hits;
        imageAdapter.upsertMany(state, entities.images || {});
      }
    );
  },
});

// Selectors
export const {
  selectById: selectImageById,
  selectIds: selectImageIds,
  selectEntities: selectImageEntities,
  selectAll: selectAllImages,
  selectTotal: selectTotalImages,
} = imageAdapter.getSelectors<RootState>((state) => state.images);

export const selectCurrentImageId = (state: RootState): number =>
  state?.images?.selectedId || -1;

export const selectCurrentImageIds = (state: RootState): number[] =>
  state?.images?.currentIds || [];

export const selectCurrentImages = createSelector(
  selectCurrentImageIds,
  selectImageEntities,
  (currentIds: number[], entities: Dictionary<Image>): Image[] => {
    return currentIds.map((id) => entities[id]!);
  }
);

export const selectSelectedImage = createSelector(
  selectCurrentImageId,
  selectImageEntities,
  (selectedId: number, entitties: Dictionary<Image>): Image | undefined => {
    return entitties[selectedId];
  }
);

// Actions
export const { setTerm, setPage, setSelectImageId } = imageSlice.actions;

export default imageSlice.reducer;
