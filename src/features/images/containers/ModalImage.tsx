import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

import Modal from "../../../shared/components/Modal";
import ImageList from "../components/ImageList";
import ImagePreview from "../components/ImagePreview";
import ImageSearch from "../components/ImageSearch";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../../lib/store";
import {
  getImageListAction,
  GetImageListPayload,
  ImageState,
  selectCurrentImages,
  setPage,
  setTerm,
  setSelectImageId,
  selectSelectedImage,
} from "../imageSlice";
import { useDebounce } from "../../../shared/hooks/useDebounce";
import { useEffect } from "react";
import ImageListPagination from "../components/ImageListPagination";
import { Image } from "../imageModel";

type Props = {
  visible: boolean;
  onSelectImage?: (imageUrl: string) => void;
  onClose?: () => void;
};

const ModalImage: React.FC<Props> = ({ visible, onSelectImage, onClose }) => {
  const dispatch = useDispatch();
  const {
    term,
    status,
    selectedId,
    hasPrevious,
    hasNext,
    page,
    totalPage,
    total,
    maximum,
  } = useSelector<RootState, ImageState>((state) => state.images);
  const imageList: Image[] = useSelector(selectCurrentImages);
  const selectedImage: Image | undefined = useSelector(selectSelectedImage);
  const [debouncedTerm, setDebouncedTerm] = useDebounce(term, 350);

  const handleSetTerm = (term: string) => {
    setDebouncedTerm(term);
  };

  const handlePageChange = (nextPage: number) => {
    dispatch(setPage(nextPage));
  };

  const handleSelectImageId = (imageId: number) => {
    dispatch(setSelectImageId(imageId));
  };

  const handleClose = () => {
    if (onClose) {
      dispatch(setSelectImageId(undefined));
      onClose();
    }
  };

  useEffect(() => {
    dispatch(setTerm(debouncedTerm));
  }, [debouncedTerm, dispatch]);

  useEffect(() => {
    const payload: GetImageListPayload = {
      term,
      page,
    };
    dispatch(getImageListAction(payload));
  }, [term, page, dispatch]);

  return (
    <Modal visible={visible}>
      <FontAwesomeIcon
        size="lg"
        className="absolute right-4 top-4 cursor-pointer"
        icon={faTimes}
        onClick={handleClose}
      />
      <div className="flex max-w-6xl">
        <div className="bg-gray-900 p-4">
          <ImageSearch
            countFound={total}
            countMaximum={maximum}
            term={term}
            onSetTerm={handleSetTerm}
          />
          <ImageList
            list={imageList}
            status={status}
            selectedId={selectedId}
            onSelectImageId={handleSelectImageId}
          />
          <ImageListPagination
            hasPrevious={hasPrevious}
            hasNext={hasNext}
            currentPage={page}
            totalPage={totalPage}
            onSetPage={handlePageChange}
          />
        </div>
        <ImagePreview
          selectedImage={selectedImage}
          onSetTerm={handleSetTerm}
          onSelectImage={onSelectImage}
        />
      </div>
    </Modal>
  );
};

export default ModalImage;
