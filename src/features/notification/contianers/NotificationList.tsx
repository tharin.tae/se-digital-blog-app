import { useSelector } from "react-redux";
import { RootState } from "../../../lib/store";
import NotificationItem from "../components/NotificationItem";

import { Notification } from "../notificationSlice";

const NotificationList: React.FC = () => {
  const notificationList = useSelector<RootState, Notification[]>(
    (state) => state.notifications
  );

  return (
    <div className="absolute top-16 right-6 z-10">
      {notificationList.map((notification: Notification) => (
        <NotificationItem
          key={notification.id}
          id={notification.id}
          type={notification.type}
          title={notification.title}
          body={notification.body}
        />
      ))}
    </div>
  );
};

export default NotificationList;
