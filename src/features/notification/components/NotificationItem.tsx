import classnames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTimes,
  faExclamationTriangle,
  faCheckCircle,
  faTimesCircle,
} from "@fortawesome/free-solid-svg-icons";
import { NotificationType } from "../notificationSlice";
import { useEffect, useState } from "react";

type Props = {
  id: string;
  timer?: number;
  type?: string;
  title?: string;
  body?: string;
};

const FIVE_SECOND = 5 * 1000;

const NotificationItem: React.FC<Props> = ({
  id,
  timer = FIVE_SECOND,
  title,
  body,
  type = NotificationType.DEFAULT,
}) => {
  const [isShow, setIsShow] = useState(true);

  const handleClose = () => {
    setIsShow(false);
  };

  useEffect(() => {
    const timeout = setTimeout(() => {
      setIsShow(false);
    }, timer);

    return () => {
      clearTimeout(timeout);
    };
  });

  if (!isShow) return null;

  return (
    <div className="relative shadow-lg rounded-lg bg-white mx-auto m-8 p-3.5 notification-box w-72">
      <FontAwesomeIcon
        icon={faCheckCircle}
        className={classnames({
          "absolute left-4 top-4 text-green-500": true,
          invisible: type !== NotificationType.SUCCESS,
          visible: type === NotificationType.SUCCESS,
        })}
      />
      <FontAwesomeIcon
        icon={faTimesCircle}
        className={classnames({
          "absolute left-4 top-4 text-red-500": true,
          invisible: type !== NotificationType.FAIL,
          visible: type === NotificationType.FAIL,
        })}
      />
      <FontAwesomeIcon
        icon={faExclamationTriangle}
        className={classnames({
          "absolute left-4 top-4 text-yellow-500": true,
          invisible: type !== NotificationType.WARNING,
          visible: type === NotificationType.WARNING,
        })}
      />
      <div className="relative ml-6">
        <div className="text-sm pb-2">
          <span className="font-medium">{title}</span>
          <span className="float-right">
            <FontAwesomeIcon icon={faTimes} onClick={handleClose} />
          </span>
        </div>
        <div className="text-sm text-gray-600  tracking-tight ">{body}</div>
      </div>
    </div>
  );
};

export default NotificationItem;
