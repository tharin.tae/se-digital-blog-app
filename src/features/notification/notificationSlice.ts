import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export enum NotificationType {
  DEFAULT = "DEFAULT",
  SUCCESS = "SUCCESS",
  FAIL = "FAIL",
  WARNING = "WARNING",
}

export type Notification = {
  id: string;
  type?: NotificationType;
  title?: string;
  body?: string;
};

const initialState: Notification[] = [];
const notificationSlice = createSlice({
  name: "notifications",
  initialState,
  reducers: {
    showNotification(state, action: PayloadAction<Notification>) {
      state.push(action.payload);
    },
  },
});

// Actions
export const { showNotification } = notificationSlice.actions;

export default notificationSlice.reducer;
